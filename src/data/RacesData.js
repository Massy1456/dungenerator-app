export const standard_races = [
    "Dwarf", "Elf", "Gnome", "Halfling", "Half-Orc", "Human", "Tiefling", "Dragonborn"
]

export const eberron_races = [
    "Changeling", "Kalashtar", "Shifter", "Warforged"
]

export const ee_races = [
    "Aarakocra", "Genasi", "Goliath"
]

export const guildmaster_races = [
    "Centaur", "Luxodon", "Minotaur", "Simic Hybrid", "Vedalken"
]

export const volo_races = [
    "Aasimar", "Bugbear", "Firbolg", "Goblin", "Hobgoblin", "Kenku", "Kobold", "Lizardfolk", "Orc", "Tabaxi", "Triton", "Yuan-Ti Pureblood"
]