export const maleNames = [
    "Markus", "Anton", "Xavier", "Bertrand", "Rudolf", "Arbitrus", "Luthor", "Ennith", "Emir", "Magnius", "Jal",
    "Sabar", "Taul", "Namor", "Odo", "Syf", "Balum", "Tamlin", "Gustall", "Alad", "Alde", "Eugan", "Aldwulf", "Killian",
    "Hadwyn", "Draec", "Balun", "Alathic", "Nikon", "Ealric", "Vyn", "Aegar", "Lothain", "Forthus", "Tassian",
]

export const femaleNames = [
    "Ari", "Alessia", "Astryn", "Bethamy", "Celestus", "Daema", "Draya", "Elanis", "Feylin", "Gwendalin", "Helena",
    "Imara", "Ifre", "Jevelyn", "Kiira", "Kotia", "Llana", "Lysabel", "Mirya", "Nyrinn", "Nessira", "Ometa", "Peyra","Quyana","Ryenne",
    "Roza", "Rikia", "Solina", "Sarabeth", "Tyra", "Tamia", "Veil", "Vainira", "Wrena", "Yeva", "Zaya" 
]