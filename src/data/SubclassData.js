export const barbarianSubclass = [
    "Path of Ancestral Guardian", 
    "Path of the Battlerager", 
    "Path of the Beast", 
    "Path of the Berserker", 
    "Path of the Storm Herald", 
    "Path of the Totem Warrior", 
    "Path of the Zealot", 
    "Path of Wild Magic"
];

export const bardSubclass = [
    "College of Creation", 
    "College of Eloquence", 
    "College of Glamour", 
    "College of Lore", 
    "College of Spirits", 
    "College of Swords", 
    "College of Valor", 
    "College of Whisper"
];

export const clericSubclass = [
    "Arcane Domain",
    "Death Domain",
    "Forge Domain",
    "Grave Domain",
    "Knowledge Domain",
    "Life Domain",
    "Light Domain",
    "Nature Domain",
    "Order Domain",
    "Tempest Domain",
    "Trickery Domain",
    "War Domain"
];

export const druidSubclass = [
    "Circle of Dreams",
    "Circle of Spores",
    "Circle of Stars",
    "Circle of Wildfire",
    "Circle of the Land",
    "Circle of the Moon",
    "Circle of the Shepherd"
];

export const fighterSubclass = [
    "Arcane Archer",
    "Battlemaster",
    "Cavalier",
    "Champion",
    "Echo Knight",
    "Eldritch Knight",
    "Psi Warrior",
    "Purple Dragon Warrior",
    "Rune Knight",
    "Samurai"
];

export const monkSubclass = [
    "Way of Shadow",
    "Way of the Ascended Dragon",
    "Way of the Astral Self",
    "Way of the Drunken Master",
    "Way of the Four Elements",
    "Way of the Kensei",
    "Way of the Long Death",
    "Way of the Open Hand",
    "Way of the Sun Soul"
];

export const paladinSubclass = [
    "Oath of Conquest",
    "Oath of Devotion",
    "Oath of Glory",
    "Oath of Redemption",
    "Oath of the Ancients",
    "Oath of the Crown",
    "Oath of the Watchers",
    "Oath of Vengeance",
    "Oathbreaker"
];

export const rangerSubclass = [
    "Beast Master",
    "Drakewarden",
    "Fey Wanderer",
    "Gloom Stalker",
    "Horizon Walker",
    "Hunter",
    "Monster Slayer",
    "Swarm Keeper",
];

export const rogueSubclass = [
    "Arcane Trickster",
    "Assassin",
    "Inquisitive",
    "Phantom",
    "Mastermind",
    "Scout",
    "Soul Knife",
    "Swashbuckler",
    "Thief"
];

export const sorcererSubclass = [
    "Aberrant Mind",
    "Clockwork Soul",
    "Divine Soul",
    "Draconic Bloodline",
    "Shadow Magic",
    "Storm Sorcery",
    "Wild Magic"
];

export const warlockSubclass = [
    "The Archfey",
    "The Celestial",
    "The Fathomless",
    "The Fiend",
    "The Genie",
    "The Great Old One",
    "The Hexblade",
    "The Undead",
    "The Undying"
];

export const wizardSubclass = [
    "Order of the Scribes",
    "School of Abjuration",
    "School of Conjuration",
    "School of Divination",
    "School of Enchantment",
    "School of Evocation",
    "School of Illusion",
    "School of Necromancy",
    "School of Transmutation",
    "War Magic"
];
