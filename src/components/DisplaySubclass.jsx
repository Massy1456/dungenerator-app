

function DisplaySubclass({index}) {

    if (index === 0){
        type = "barbarian"
    }

    return (
        <div className="text-xl font-bold text-black">Subclass: <span className="font-semibold">{`${type}Subclass[subclassIndex]`}</span></div>
    )
}

export default DisplaySubclass