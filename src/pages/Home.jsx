import { roles } from '../data/ClassData'
import { maleNames, femaleNames } from '../data/NameData'
import { barbarianSubclass, bardSubclass, clericSubclass, druidSubclass, fighterSubclass, monkSubclass, paladinSubclass, rangerSubclass, 
            rogueSubclass, sorcererSubclass, warlockSubclass, wizardSubclass
} from '../data/SubclassData'
import { standard_races, eberron_races, ee_races, guildmaster_races, volo_races } from '../data/RacesData'

import { useState } from 'react'

function Home() {

    const [toggleSettings, setToggleSettings] = useState(false)
    const [maleChecked, setMaleChecked] = useState(false)
    const [femaleChecked, setFemaleChecked] = useState(false)

    const [toggleVolo, setToggleVolo] = useState(true)
    const [toggleEberron, setToggleEberron] = useState(true)
    const [toggleElementalE, setToggleElementalE] = useState(true)
    const [toggleGuildmaster, setToggleGuildmaster] = useState(true)

    const [nameIndex, setNameIndex] = useState(0)
    const [nameIndex2, setNameIndex2] = useState()
    const [classIndex, setClassIndex] = useState(0)
    const [subclassIndex, setSubclassIndex] = useState(0)
    const [raceIndex, setRaceIndex] = useState(0)
    const [race, setRace] = useState(standard_races)

    const generate = () => {
        let index = Math.floor(Math.random() * (maleNames.length))
        setNameIndex(index)
        index = Math.floor(Math.random() * (femaleNames.length))
        setNameIndex2(index)
        index = Math.floor(Math.random() * (roles.length))
        setClassIndex(index)     

        if(classIndex === 0){
            const index = Math.floor(Math.random() * ((barbarianSubclass.length) - 1))
            setSubclassIndex(index)
        } else if(classIndex === 1){
            const index = Math.floor(Math.random() * ((bardSubclass.length) - 1))
            setSubclassIndex(index)
        } else if(classIndex === 2){
            const index = Math.floor(Math.random() * ((clericSubclass.length) - 1))
            setSubclassIndex(index)
        } else if(classIndex === 3){
            const index = Math.floor(Math.random() * ((druidSubclass.length) - 1))
            setSubclassIndex(index)
        } else if(classIndex === 4){
            const index = Math.floor(Math.random() * ((fighterSubclass.length) - 1))
            setSubclassIndex(index)
        } else if(classIndex === 5){
            const index = Math.floor(Math.random() * ((monkSubclass.length) - 1))
            setSubclassIndex(index)
        } else if(classIndex === 6){
            const index = Math.floor(Math.random() * ((paladinSubclass.length) - 1))
            setSubclassIndex(index)
        } else if(classIndex === 7){
            const index = Math.floor(Math.random() * ((rangerSubclass.length) - 1))
            setSubclassIndex(index)
        } else if(classIndex === 8){
            const index = Math.floor(Math.random() * ((rogueSubclass.length) - 1))
            setSubclassIndex(index)
        } else if(classIndex === 9){
            const index = Math.floor(Math.random() * ((sorcererSubclass.length) - 1))
            setSubclassIndex(index)
        } else if(classIndex === 10){
            const index = Math.floor(Math.random() * ((warlockSubclass.length) - 1))
            setSubclassIndex(index)
        } else if(classIndex === 11){
            const index = Math.floor(Math.random() * ((wizardSubclass.length) - 1))
            setSubclassIndex(index)
        }

        index = Math.floor(Math.random() * (race.length - 1))
        setRaceIndex(index)
        
    }

    const changeVolo = () => {
        setToggleVolo(!toggleVolo)
        if(toggleVolo){ // if volo is on, add it to the races, else delete it from the races
            setRace([...race, ...volo_races])
        } else if(!toggleVolo){
            setRace(race.filter( function( el ) {
                return !volo_races.includes( el );
            }))
        }
    }

    const changeEberron = () => {
        setToggleEberron(!toggleEberron)
        if(toggleEberron){ // if volo is on, add it to the races, else delete it from the races
            setRace([...race, ...eberron_races])
        } else if(!toggleEberron){
            setRace(race.filter( function( el ) {
                return !eberron_races.includes( el );
            }))
        }
    }

    const changeElementalE = () => {
        setToggleElementalE(!toggleElementalE)
        if(toggleElementalE){ // if volo is on, add it to the races, else delete it from the races
            setRace([...race, ...ee_races])
        } else if(!toggleElementalE){
            setRace(race.filter( function( el ) {
                return !ee_races.includes( el );
            }))
        }
    }

    const changeGuildmaster = () => {
        setToggleGuildmaster(!toggleGuildmaster)
        if(toggleGuildmaster){ // if volo is on, add it to the races, else delete it from the races
            setRace([...race, ...guildmaster_races])
        } else if(!toggleGuildmaster){
            setRace(race.filter( function( el ) {
                return !guildmaster_races.includes( el );
            }))
        }
    }

    const handleToggle = () => {
        setToggleSettings(!toggleSettings)

        if(!toggleSettings){
            if (!toggleEberron){
                setToggleEberron(true)
            }
            if (!toggleElementalE){
                setToggleElementalE(true)
            }
            if (!toggleGuildmaster){
                setToggleGuildmaster(true)
            }
            if (!toggleVolo){
                setToggleVolo(true)
            }
            if(maleChecked){
                setMaleChecked(false)
            }
            if(femaleChecked){
                setFemaleChecked(false)
            }
        }
    }

        return (
            <div className="container mx-auto">
                <div className="p-6 space-x-4 max-w-sm mx-auto bg-white rounded-md shadow-lg flex items-center">
                    <ul>
                        <li className="pb-5">
                            {(!maleChecked && !femaleChecked) && (<div className="text-xl font-bold text-black">Name: </div>)}
                            {(maleChecked && !femaleChecked) && (<div className="text-xl font-bold text-black">Name: <span className="font-semibold">{maleNames[nameIndex]}</span></div>)}
                            {(femaleChecked && !maleChecked) && (<div className="text-xl font-bold text-black">Name: <span className="font-semibold">{femaleNames[nameIndex]}</span></div>)}
                            {(femaleChecked && maleChecked) && (<div className="text-xl font-bold text-black">Name: <span className="font-semibold">{maleNames[nameIndex]}, {femaleNames[nameIndex]}</span></div>)}     
                        </li>
                        <li className="pb-5">
                            <div className="text-xl font-bold text-black">Class: <span className="font-semibold">{roles[classIndex]}</span></div>
                        </li>
                        <li className="pb-5">
                            {(classIndex === 0) && (<div className="text-xl font-bold text-black">Subclass: <span className="font-semibold">{barbarianSubclass[subclassIndex]}</span></div>)}
                            {(classIndex === 1) && (<div className="text-xl font-bold text-black">Subclass: <span className="font-semibold">{bardSubclass[subclassIndex]}</span></div>)}
                            {(classIndex === 2) && (<div className="text-xl font-bold text-black">Subclass: <span className="font-semibold">{clericSubclass[subclassIndex]}</span></div>)}
                            {(classIndex === 3) && (<div className="text-xl font-bold text-black">Subclass: <span className="font-semibold">{druidSubclass[subclassIndex]}</span></div>)}
                            {(classIndex === 4) && (<div className="text-xl font-bold text-black">Subclass: <span className="font-semibold">{fighterSubclass[subclassIndex]}</span></div>)}
                            {(classIndex === 5) && (<div className="text-xl font-bold text-black">Subclass: <span className="font-semibold">{monkSubclass[subclassIndex]}</span></div>)}
                            {(classIndex === 6) && (<div className="text-xl font-bold text-black">Subclass: <span className="font-semibold">{paladinSubclass[subclassIndex]}</span></div>)}
                            {(classIndex === 7) && (<div className="text-xl font-bold text-black">Subclass: <span className="font-semibold">{rangerSubclass[subclassIndex]}</span></div>)}
                            {(classIndex === 8) && (<div className="text-xl font-bold text-black">Subclass: <span className="font-semibold">{rogueSubclass[subclassIndex]}</span></div>)}
                            {(classIndex === 9) && (<div className="text-xl font-bold text-black">Subclass: <span className="font-semibold">{sorcererSubclass[subclassIndex]}</span></div>)}
                            {(classIndex === 10) && (<div className="text-xl font-bold text-black">Subclass: <span className="font-semibold">{warlockSubclass[subclassIndex]}</span></div>)}
                            {(classIndex === 11) && (<div className="text-xl font-bold text-black">Subclass: <span className="font-semibold">{wizardSubclass[subclassIndex]}</span></div>)}
                        </li>
                        <li className="pb-5">
                            <div className="text-xl font-bold text-black">Race: <span className="font-semibold">{race[raceIndex]}</span></div>
                        </li>
                        <li className="pb-5">
                            <button onClick={generate} className="btn btn-primary btn-md rounded-btn">Generate</button>
                        </li>
                        <li>
                            <div>
                                <button onClick={handleToggle} className="btn btn-sm rounded-md bg-accent-content border-gray-200 font-bold text-md text-black hover:text-black hover:bg-slate-200">Settings</button>  
                            </div>
                        </li>
                        {toggleSettings && (
                        <>
                            <li className="grid grid-col-1">
                                <div className="font-bold text-md pb-2 mt-3 underline underline-offset-1 text-black">Genders:</div>  
                            </li>
                            <li className="pb-1">
                                <div className="flex">
                                <div className="grid grid-cols-2">
                                    <div className="form-check mb-3">
                                        <input onChange={() => setMaleChecked(!maleChecked)} className="form-check-input appearance-none h-5 w-5 border border-gray-300 rounded-sm bg-white checked:bg-primary checked:border-white focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="checkbox" value="" id="flexCheckDefault"/>
                                        <label className="form-check-label inline text-gray-800 font-semibold" for="flexCheckDefault">
                                            Male
                                        </label>
                                    </div>
                                    <div className="form-check">
                                        <input onChange={() => setFemaleChecked(!femaleChecked)} className="form-check-input appearance-none h-5 w-5 border border-gray-300 rounded-sm bg-white checked:bg-primary checked:border-white focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="checkbox" value="" id="flexCheckChecked"/>
                                        <label className="form-check-label inline text-gray-800 font-semibold" for="flexCheckChecked">
                                            Female
                                        </label>
                                    </div>
                                    </div>
                                </div>
                            </li>
                            <li className="grid grid-col-1">
                                <div className="font-bold text-md pb-2 underline underline-offset-1 text-black">Include Expansions:</div>  
                            </li>
                            <li className="pb-1">
                                <div className="flex">
                                <div className="grid grid-cols-2">
                                    <div className="form-check mb-3">
                                        <input onChange={changeVolo} className="form-check-input appearance-none h-5 w-5 border border-gray-300 rounded-sm bg-white checked:bg-primary checked:border-white focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="checkbox" value="" id="flexCheckDefault"/>
                                        <label onChange={changeVolo} className="form-check-label inline text-gray-800 font-semibold">
                                            Volo's Guide
                                        </label>
                                    </div>
                                    <div className="form-check pl-2">
                                        <input onChange={changeEberron} className="form-check-input appearance-none h-5 w-5 border border-gray-300 rounded-sm bg-white checked:bg-primary checked:border-white focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="checkbox" value="" id="flexCheckChecked"/>
                                        <label className="form-check-label inline text-gray-800 font-semibold">
                                            Eberron
                                        </label>
                                    </div>
                                    </div>
                                </div>
                                <div className="flex">
                                <div className="grid grid-cols-2">
                                    <div className="form-check">
                                        <input onChange={changeElementalE} className="form-check-input appearance-none h-5 w-5 border border-gray-300 rounded-sm bg-white checked:bg-primary checked:border-white focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="checkbox" value="" id="flexCheckDefault"/>
                                        <label className="form-check-label inline text-gray-800 font-semibold">
                                            Elemental Evil
                                        </label>
                                    </div>
                                    <div className="form-check pl-2">
                                        <input onChange={changeGuildmaster} className="form-check-input appearance-none h-5 w-5 border border-gray-300 rounded-sm bg-white checked:bg-primary checked:border-white focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="checkbox" value="" id="flexCheckChecked"/>
                                        <label className="form-check-label inline text-gray-800 font-semibold">
                                            Guildmaster's
                                        </label>
                                    </div>
                                    </div>
                                </div>
                            </li>
                        </>
                        )}
                    </ul>
                </div>
            </div>
    
        )
    
}

export default Home
