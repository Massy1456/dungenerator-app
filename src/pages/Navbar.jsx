import { Link } from 'react-router-dom'
import { SiDungeonsanddragons } from 'react-icons/si'

function Navbar() {
  
    return (
        <nav className="navbar mb-12 shadow-lg bg-neutral text-neutral-content">
            <div className="container mx-auto">
                <div className="flex-none px-2 mx-2">
                <SiDungeonsanddragons className="inline pr-2 text-3xl"/>
                    <span className="text-lg font-bold align-middle">DunGenerator</span>
                </div>
                <div className="flex-1 px-2 mx-2">
                    <div className="flex justify-end">
                        <Link to='/' className="btn btn-ghost btn-sm rounded-btn mr-5">
                            Home
                        </Link>
                        <Link to='/about' className="btn btn-ghost btn-sm rounded-btn mr-5">
                            About
                        </Link>
                    </div>
                </div>
            </div>
        </nav>
    )

}

export default Navbar
